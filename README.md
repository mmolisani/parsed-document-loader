# parsed-document-loader

Simple webpack loader that imports html/xml/svg files as DOM `Document` objects using `DOMParser`

The `mimeType` is determined automatically from the extension but can also be overridden as an option (`mimetype`/`mimeType` both supported)
