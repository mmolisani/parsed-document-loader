
declare module "*.html" {
  const document: Document;
  export = document;
}

declare module "*.xml" {
  const document: Document;
  export = document;
}

declare module "*.svg" {
  const document: Document;
  export = document;
}
