
const { getOptions } = require("loader-utils");
const mime = require("mime");

module.exports = function(content) {
  this.cacheable && this.cacheable();
  const options = getOptions(this) || {};
  const mimeType = options.mimetype || options.mimeType || mime.getType(this.resourcePath);
  const documentContent = JSON.stringify(content).replace(/\u2028/g, "\\u2028").replace(/\u2029/g, "\\u2029");
  return `module.exports = (function(){
    return (new DOMParser()).parseFromString(${documentContent}, ${JSON.stringify(mimeType)});
  })();`;
};
